#import class modules
import circle_class as cc
import triangle_class as tc


#instantiate a circle and a triangle obect

circ = cc.Circle()
tri = tc.Triangle()


#initialize attributes
tri.height = 1
tri.width = 0.5
circ.radius = 2


#compute / print areas for the two objects
print circ.getArea()
print tri.getArea()